type User = {
  name: string;
  age: number;
  parents?: string[];
};

const getUser = (id: string): Promise<User> => {
  switch (id) {
    case 'parent1':
      return Promise.resolve({ name: 'Dad', age: 34 });
    case 'parent2':
      return Promise.resolve({ name: 'Mom', age: 31 });
    default:
      return Promise.resolve({ name: 'Timmy', age: 19, parents: ['parent1', 'parent2'] });
  }
};

const logUser = (user: User): Promise<void> =>
  new Promise(resolve => {
    console.log(`name: ${user.name}`);
    console.log(`age: ${user.age}`);
    resolve();
  });

const myFunction3 = (): void => {
  getUser('timmy1').then(user => {
    if (user.age <= 18) {
      return Promise.all(user.parents!.map(parentId => getUser(parentId).then(logUser))).then(() => {
        console.log('user and parents');
      });
    }
    return logUser(user).then(() => {
      console.log('user');
    });
  });
};

myFunction3();
