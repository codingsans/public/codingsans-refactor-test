const myPromise2 = (): Promise<string> => Promise.resolve('promise1');
const myFunction2 = (): void => {
  console.log('promise resolved: ', myPromise2());
};

myFunction2();
