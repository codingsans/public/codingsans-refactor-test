const myPromise1 = (x: number): Promise<void> =>
  new Promise((resolve, reject) => {
    if (x < 5) {
      reject();
    }
    console.log('promise is running ...');
    resolve();
  });

// first case
myPromise1(6)
  .then(() => console.log('resolved'))
  .catch(() => console.log('rejected'));

// second case
myPromise1(3)
  .then(() => console.log('resolved'))
  .catch(() => console.log('rejected'));
