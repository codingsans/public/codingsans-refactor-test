module.exports = {
  extends: ['@codingsans/eslint-config/typescript-recommended'],
  rules: {
    complexity: ['error', 12],
    '@typescript-eslint/no-floating-promises': 1,
  },
};
