# CodingSans Refactor Test

## Example 1 - src/example1.ts

What is the problem with this code?
Why `promise is running ...` is logged in the second case?
Refactor the code by adding 1 keyword to reach the proper behavior.

Write your solution to: src/example1-solution.ts
Test your code by `yarn start:example1`

## Example 2 - src/example2.ts

MyFunction should write `promise resolved: promise1` to the console. To reach this you can only use 2 keywords.

Write your solution to: src/example2-solution.ts
Test your code by `yarn start:example2`

## Example 3 - src/example2.ts

The goal is to avoid the Promise christmas trees and keep the same business logic in the code.
To reach this use async/ await in the refactor.

Write your solution to: src/example3-solution.ts
Test your code by `yarn start:example3`
